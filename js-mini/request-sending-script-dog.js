// Chris Hunt
// Jack Bigej
// Andrew Rocks

console.log("started script!");
// get button
send_button = document.getElementById("theButton");

// text value
send_button.onmouseup = httpCall;

function httpCall() {
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "https://dog.ceo/api/breeds/image/random", true);

    xhr.onload = function() {
        if (xhr.readyState === 4){
            var img = new Image();
            var response_dict = JSON.parse(xhr.responseText);
            var src = response_dict["message"];
            img = document.getElementById("response_img");
            img.src = src;

        }else{
            console.error(xhr.statusText);
        }
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send();

}
