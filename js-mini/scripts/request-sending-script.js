// Chris Hunt
// Jack Bigej
// Andrew Rocks

console.log("started script!");
// get button
send_button = document.getElementById("theButton");

// text value
var hero_num = document.getElementById('input-hero-number').value;

send_button.onmouseup = httpCall;

function httpCall() {
    var xhr = new XMLHttpRequest()

    xhr.open("GET", "http://student04.cse.nd.edu:51031/heroes/" + hero_num, true)

    xhr.onload = function(e) {
        console.log(xhr.responseText)
        responseLabel.innerHTML = xhr.responseText;
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(null)

}
